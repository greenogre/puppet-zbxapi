class zbxapi::params {
  
  validate_bool( $zbxapi::pkg_manage )
  
  $pkg_ensure   = present
  $pkg_provider = 'gem'
  $pkg_depends  = [ 'ruby-dev', 'make', ]
  
}
