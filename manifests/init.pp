class zbxapi (
  $url,
  $user,
  $password,
  $pkg_manage = false,
) {
  
  include zbxapi::params
  include zbxapi::install

  file {"${::settings::confdir}/zbxapi.yaml":
    owner => "root",
    group => "root",
    mode => "0600",
    content => template("zbxapi/zbxapi.yaml.erb")
  }
}
