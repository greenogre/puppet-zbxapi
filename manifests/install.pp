class zbxapi::install { 
  
  if $zbxapi::pkg_manage {
  
    ensure_packages($zbxapi::params::pkg_depends)

    package { 'zbxapi':
      ensure   => $zbxapi::params::pkg_ensure,
      provider => "${zbxapi::params::pkg_provider}",
      require  => Package[ $zbxapi::params::pkg_depends ],
    }
  }
}
