require 'puppet/parameter/boolean'

Puppet::Type.newtype(:zabbix_proxy) do
  desc "Manage a Zabbix proxy"
  ensurable do
    defaultvalues
    defaultto :present
  end

  newparam(:name, :namevar => true) do
    desc "Proxy hostname"
  end
  
  newproperty(:proxymode ) do               # AKA "status"
    desc "Proxy mode: active or passive. (default: active)"
    newvalues(:active, "active", :true, :passive, "passive", :false)
    
    munge do |value|
      case value
        when :true, :active, "active"
          :active
        when :false, :passive, "passive"
          :passive
      end
    end
    
    defaultto :active
  end
  
  newproperty(:description) do
    desc "Description of proxy."
    
    validate do |value|
      fail("Invalid description #{value}") unless value =~ /^[0-9A-Za-z\.,-_\s]*$/
    end
    
    defaultto ""
  end
     
  newproperty(:connect) do            # AKA "useip"
    desc "Use IP or DNS to connect to proxy host. (default: ip)"
    newvalues("ip", :ip, :true, "dns", :dns, :false)
    
    munge do |value|
      if resource[:proxymode] == :active
        nil
      else
        case value
        when :true, "ip", :ip
          :ip
        when :false, "dns", :dns
          :dns
        end
      end
    end
  end
  
  newproperty(:ipaddr) do
    desc "IP address for passive mode."
    
    munge do |value|
      if resource[:proxymode] == :active
        nil
      else
        value 
      end
    end
    
    defaultto ""
  end
   
  newproperty(:dnsname) do
    desc "DNS name for passive mode. (default: proxy name)"
    
    munge do |value|
      if resource[:proxymode] == :active
        nil
      else
        value 
      end
    end
    
    defaultto { resource[:name] } 
  end
   
  newproperty(:port) do
    desc "Port to connect to for passive mode. (default: 10051)"
    newvalues( /^[\d]{1,5}$/ )
    
    munge do |value|
      if resource[:proxymode] == :active
        nil
      end
    end
  end
  
  validate do
    if self[:proxymode] == :passive and self[:ensure] == :present and self[:connect] == :ip
      fail("IP address is required when proxy connect mode is \"ip\".") if self[:ipaddr].nil? or self[:ipaddr].empty?
      
    end
  end
end
