Puppet::Type.newtype(:zabbix_hostgroup) do
  desc "Manage a Zabbix hostgroup"
  
  ensurable do
    defaultvalues
    defaultto :present
  end

  newparam(:name, :namevar => true) do
    desc "Hostgroup name"
  end

end
