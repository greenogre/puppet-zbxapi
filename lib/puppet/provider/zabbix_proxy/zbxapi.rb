require File.expand_path(File.join(File.dirname(__FILE__), "..", "..", "..", "ruby", "zbxapi.rb"))

Puppet::Type.type(:zabbix_proxy).provide(:zbxapi) do
  desc "Zabbix proxy zbxapi provider"
  confine :feature => :zbxapi
    
  def self.instances
    extend ZbxAPI
    
    proxies = zbxapi.proxy.get({
      "output" => ["host", "status", "description", "proxyid", "extend", ],
      "selectInterface" => [ "interfaceid", "ip", "dns", "useip", "port", ]
    })
    proxies.collect do |proxy|
      case proxy["status"]
        when "6"
          ifparams = proxy["interface"]
          if !ifparams.empty?
          t_interfaceid = ifparams["interfaceid"]
          t_ipaddr      = ifparams["ip"]
          t_dnsname     = ifparams["dns"]
          t_connect     = ifparams["useip"] == "0" ? :dns : :ip
          t_port        = ifparams["port"]
          end
        when "5"
          t_interfaceid = nil
          t_ipaddr      = ""
          t_dns         = ""
          t_connect     = nil
          t_port        = ""
      end
      new(
        :name        => proxy["host"],
        :host        => proxy["host"],
        :status      => proxy["status"],
        :proxymode   => proxy["status"] == "6" ? :passive : :active,
        :description => proxy["description"],
        :proxyid     => proxy["proxyid"], 
        :interface   => proxy["interface"],
        :interfaceid => t_interfaceid,
        :ipaddr      => t_ipaddr,
        :dnsname     => t_dnsname,
        :connect     => t_connect,
        :port        => t_port,
        :ensure      => :present
      )
    end
  end
  
  def self.prefetch(resources)
    extend ZbxAPI
    proxies = instances
    resources.keys.each do |name|
      if provider = proxies.find { |proxy| proxy.name == name }
        resources[name].provider = provider
      end
    end
  end

  def exists?
    @property_hash[:ensure] == :present
  end
  
  def set_ifparams    
    # Set interface values
    ifport    = @resource[:port] == nil ? "10051" : @resource[:port].to_s
    ifconnect = @resource[:connect] == :dns ? "0" : "1"
    
    ifparams = { "useip" => "#{ifconnect}", "port"  => "#{ifport}", }
    
    if @resource[:ipaddr] != nil 
      ifparams.merge!( "ip" => @resource[:ipaddr] )
    else
      ifparams.merge!( "ip" => "" )
    end
    
    if @resource[:dnsname] != nil
      ifparams.merge!( "dns" => @resource[:dnsname] )
    else
      ifparams.merge!( "dns" => "" )
    end
    
    if @property_hash[:interfaceid] != nil
      ifparams.merge!( "interfaceid" => @property_hash[:interfaceid] )
    end
    ifparams
  end
  
  def create
    extend ZbxAPI
    
    hostparams = {
      "host"        => @resource[:name],
      "status"      => @resource[:proxymode] == :passive ? "6" : "5",
      "description" => @resource[:description],
    }

    # Get interface values only if passive proxy
    if @resource[:proxymode] == :passive
      
      hostparams.merge!( "interface" => set_ifparams )
    end

    zbxapi.proxy.create( hostparams )

    @property_hash[:ensure] = :present
  end

  def destroy
    extend ZbxAPI

    zbxapi.proxy.delete( [ @property_hash[:proxyid] ] )
    @property_hash.clear
  end

# Using mk_resource_methods relieves us from having to explicitly write the getters for all properties
  mk_resource_methods
  
  def initialize(value = {})
    super(value)
    @property_flush = {}
  end
  
  def proxymode=(value)
    @property_flush[:status] = @resource[:proxymode] == :passive ? "6" : "5"
  end
  
  def description=(value)
    @property_flush[:description] = value
  end
    
  def flush
    extend ZbxAPI
    
    # Get interface values only if passive proxy that already exists
    if @resource[:proxymode] == :passive and @property_hash[:proxyid] != nil
    
      @property_flush["interface"] = set_ifparams #ifproperties #_hash
    end
    
    unless @property_flush.empty?
      @property_flush["proxyid"] = @property_hash[:proxyid]
      #notes = @property_flush
      #warning( "apicmd:#{notes}:")
      zbxapi.proxy.update(@property_flush)
    end
    
    @property_hash = resource.to_hash
  end

end

