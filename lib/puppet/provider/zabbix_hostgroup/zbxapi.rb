require File.expand_path(File.join(File.dirname(__FILE__), "..", "..", "..", "ruby", "zbxapi.rb"))

Puppet::Type.type(:zabbix_hostgroup).provide(:zbxapi) do
  desc "Zabbix hostgroup type"
  confine :feature => :zbxapi

  def self.instances
    extend ZbxAPI

    hostgroups = zbxapi.hostgroup.get({"output" => ["name", "groupid"]})
    hostgroups.collect do |hostgroup|
      new(
        :name => hostgroup["name"],
        :groupid => hostgroup["groupid"],
        :ensure => :present
      )
    end
  end

  def self.prefetch(resources)
    extend ZbxAPI

    hostgroups = instances
    resources.keys.each do |name|
      if provider = hostgroups.find { |hostgroup| hostgroup.name == name }
        resources[name].provider = provider
      end
    end
  end

  def exists?
    @property_hash[:ensure] == :present
  end

  def create
    extend ZbxAPI

    zbxapi.hostgroup.create({
      "name" => resource[:name],
    })

    @property_hash[:ensure] = :present
  end

  def destroy
    extend ZbxAPI

    zbxapi.hostgroup.delete("groupid" => @property_hash[:groupid])
    @property_hash.clear
  end

  # Using mk_resource_methods relieves us from having to explicitly write the getters for all properties
  mk_resource_methods

  def initialize(value = {})
    super(value)
    @property_flush = {}
  end

  def flush
    extend ZbxAPI

    unless @property_flush.empty?
      @property_flush["groupid"] = @property_hash[:groupid]
      zbxapi.hostgroup.update(@property_flush)
    end
    @property_hash = resource.to_hash
  end
end

